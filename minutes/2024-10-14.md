﻿Harrison Neighborhood Association
============================================
**Board Meeting Minutes**
**Monday, Oct. 14th 2024**
**7pm-9pm**

# 7:00 Call to Order/Role Call/Introductions
- we have quorum
 
# 7:08 Brief Summary of the Standard of Conduct and HNA Mission - Dustin Ruff

# 7:10 Review and Approve Agenda
- Dustin motion to add, Dan 2nd motion passes
  - Neighborhood and Community Relations (NCR) board representation survey
- Dan makes motion to approve, Robert 2nds, motion approved

# 7:12 Neighborhood and Community Relations (NCR) board representation survey 

# 7:15 Review and Approve Previous Months Minutes
- Any suggested changes?

# 7:19-8:15 ED + Community Outreach Director Report
- Re-thinking I-94/394 "elbow" meetings ~1/month
- Mitchell talked about Emerald Ash Borer work
  - Cargill donated $500k, Black Visions Collective gave $140k from Rhianna, and others to help remove liens against homes
  - Working on reimbursement through dept of public health to get money ~$32k for our work
  - We have replacement trees and/or talk to Metro Blooms
- Potential to hire new person came up in discussion, current funding levels don't really allow it
- Nichole explaining programs
  - Home Improvement Grants
    - Mitchell has been helping with applications and connecting w/ contractors
    - NRP funding is almost out :( may be able to replenish with "Make Harrison Whole" campaign
  - Down Payment Assistanc
  - Sustainable Resources Pilot Program
  - Tool Lending Library
  - Community Gardens in partnership with Youth Farm
- Recent Event "Playing for the Plants"
- Harrison Legal Aid Clinic
- Events
  - Earth day
  - Annual Membership Meeting
  - Summer Celebration (Imagine 6th, Open Streets)
  - Halloween
  - Harrison Suppertime
  - 40th/41st(?) Anniversary

# 8:16 Committee Structure/Workgroup Updates

## Committees (recurring and consistent):
- Events and Communications - Dustin Ruff
  - Suppertime meetings to resume
  - 40th fundraiser? 41st?
    - lots of good ideas for ways to raise money at an event
    - Dustin's going to setup a meeting to get in motion
- Finance/Treasurer's Report - Lawrence Graves
  - Got checks signed/sent
    - window replacement
  - Working with Robert to get 990 form filled out(due Nov. 15th)
- Housing and Development - Ben Ptacek
  - Nichole reached out to David Wellington (real estate developer)
  - ROC is having meetings
  - Dan explained some work on rebuilding of old Mill located near Utepils
  - We recently sent letter of support for PennGlenn development with James Archer
  - Sewer Lift station, no news

## Workgroups (Temporary):	
- Strategic Plan Workgroup
  - who's in? and when?

# 9:00 Motion to Adjourn Dan approved
