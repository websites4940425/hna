﻿Harrison Neighborhood Association
Monthly Board Meeting
Monday, November 13th 2023 
7pm-9pm

Suggested Agenda from Nov. Executive Committee meeting consisting of Treasurer/Secretary Ryan S. Davis, President Tim Davis, Executive Director Nichole Buehler, and President Elect Dustin Ruff

7:00 Call to Order/Role Call/Introductions
  -> Tim and Laura have resigned
		-> Dustin is President per bylaws -> we have no Vice President now
		-> the board is now authorized to fill that position
	-> Quorom rules may apply and be difficult
		-> bylaws 50% + 1, at least 2 officers, and 6 total
	
	==> Need to fix bylaws, update quorom rules to remove "6 total"

	Attendance, WE HAVE QUOROM

7:05 Brief Summary of the Standard of Conduct and HNA Mission - Dustin Ruff

7:10 Review and Approve Agenda
  -> Sidewalk salt and Toilet Paper - Mitch
		-> Nichole can buy under 300$, can get it
	Dustin motion, Mary 2nd approved

7:15 Review and Approve Previous Months Minutes September AND October
	-> Dustin motion to approve, Ryan 2nd
		-> vote succeeded

7:25 Attendance check discussion
	-> review rules
	-> review current attendance
	-> discuss actions

	==> we need to send written letters to all board members who have missed 2 or 3 meetings
		-> should mention importance of quoruom with slim board
	==> potential bylaws update
    -> make exceptions for family emergencies, medical emergencies, etc.
    -> if people are removed due to absences, need to figure out how to fill it or something else

	==> updating the bylaws has become an very high priority	
		-> need rules for updating bylaws
		-> need to understand contradictions in our current bylaws
		-> Ryan schedule meeting mid-December 
	
8:00 Executive Director's Report
	-> Olson Highway Update, Bring Back 6
		-> MNDOT community connectors
	-> Emerald Ash Bore Update
		-> city countil passed resolution gives people the opportunity to use pesticides
		-> secured $8 million for assistance from Inflation Reduction Act (IRA)
		-> Mitch has done a lot of groundwork on this issue :)
	-> Headwaters Well Spring Grant Update 
		-> potential $90k for 4 years (each year) to support more staff and bring back 6 initiative
		-> funding for 4 organizations
		-> we made it to final ~10 applications out of >100
  -> Met Council and Lift Station
		-> reached out to Juxtaposition to do community engagement 
		-> Juxtaposition directed them to Nichole, she's meeting with them in-person
		-> Dustin mentioned that the lift station location we suggested has been bought by the city
	-> Approved 990 and AG report and has not been submitted (Tim did not sign)
		=> Before Nov 15th, Dustin needs to sign 990, Ryan needs to sign AG report (look at their emails)
	-> Robert Thompson sent report for the wrong neighborhood today, will need to get new one
	-> if Open Streets gets funded, they're looking at Harrison again
		-> will be doing and event with Our Streets no matter what

8:16 Committe Structure/Workgroup Updates
	Committees (recurring and consistent):
		- Personnel Committee	
			Dustin, Ryan, McKenzie
			-> working on executive director's review

		- Events and Communications - Dustin Ruff
			-> Markus Car working on program for Gardens and potential money for it
			-> Dec 7th at location TBD
			-> website updates 
			-> logo re-design
				- design in office from Katherine?
			-> HNA 40th anniversary ?

		- Finance/Treasurer's Report - Ryan Davis
			-> nrp grant $$$ distributed
			-> ballet folklorico payment (?)
				-> Our Streets has paid
			-> Dustin adding, mentioned email from Gigi for Loppet event has reciepts for $200 we allotted last meeting

		- Housing and Development - Ben Ptacek
			-> lift station
				-> had a draft written statement that we want the lift station, want the other location (not on Glenwood)
				-> as mention in ED report, Nichole is meeting in-person
			-> developer came to last meeting
				-> has developed in N Minneapolis
				-> has site control over PennWood and has buyout agreement
					-> looking to build a mix-use with grocery component
						- underground parking
						- all affordable housing residential
					-> interested in doing presentation to the board
					-> may not have financing currently 
			-> next meeting would be likely in January		

		- Crime and Safety - Tim Davis has resigned, this will be defunct for now, subsumed into other commitee's work

	Workgroups (Temporary):	
		- Audit -> Dustin
			-> Meet with Dan sometime in the near future
		- Bylaws -> Ryan need to schedule our first bylaw workgroup meeting
		- Strategic Plan -> this is now getting tabled due to the lack of board members and importance of bylaws update

	Mckenzie mentioning Redeemer Center for Life (501c) (distinguished from the church) meeting for listening session 
		-> Tuesday 6:30pm, Zoom meeting
		-> RCFL living room(next to Milda's) Saturday	10am

9:00 Adjourn
Dan motion, Ryan 2nd, approved
