﻿Harrison Neighborhood Association
Monthly Board Meeting
Monday, Sept. 9th 2024 
7pm-9pm

Suggested Agenda from March Executive Committee meeting consisting of Secretary Ryan S. Davis, President Dustin Ruff, E.D. Nichole Beuhler, and  Lawrence Graves (Treasurer)

7:00 Call to Order/Role Call/Introductions
	-> did longer than usual intros for some new people

7:05 Brief Summary of the Standard of Conduct and HNA Mission - Dustin Ruff

7:10 Review and Approve Agenda
	-> Andy motion to approve, Robert 2nds, unanimously passes

7:15 Review and Approve Previous Months Minutes
	-> updated Thea's name to Thia
	-> Andy motion, Lawrence 2nd, unanimously passes

7:25-8:25 ED Report
	Paper was given out with a lot of info, 4.5 pages front and back

  Blue Line Coalition
		-> historical context of happenings
		-> have 2 seats on the corridor management committee meets monthly
		-> have sub-committees
		-> Carla Erradando(?) is now chair(?)
		-> meet monthly
		-> showed a video of BLC

	Anti-Displacement Work Group/Community Prosperity Board
		-> was created from BLC	
		-> Nichole currently serves on the Bylaws Committee and Funding Implementation Committee
		-> $10M has been allocated
			-> these committees need to be setup legitimately for money to be disbursed, hopefully solidifed by EOY
			-> any money needs a philanthropic match
		-> money will be spread over areas that are in a 1 mile radius from old and new planned route
		-> there are funders that are on the Board that can match/have matched

	Bring Back 6th
		-> HNA and Our Streets created this together
			-> HNA has anti-displacement experience
			-> Our Streets has transportation policy experience
		-> securing $400k match for Biden admin Reconnecting Communities Grant
		-> monthly meetings, possibly moving to weekly

8:25 Committee Structure/Workgroup Updates
	Committees (recurring and consistent):
		- Events and Communications - Dustin Ruff
			-> 40th fundraiser in the works
			-> 28th of Sept. Peace Haven Gardens around 5pm
		- Finance/Treasurer's Report - Lawrence Graves
			-> need to prepare 990 form
			-> Incoming Grants
				- $31k emerald ash borer
				- $10k NRP
				- $7k from Our Streets
				- $3k from MNDOT
			-> NRP status of home improvement grant funds unkown
			-> Grant application for home improvements
				-> Ryan makes motion to accept for cleanings and kitchen update supplies, Robert 2nds, unanimously approved
		- Housing and Development - Ben Ptacek
			-> letters from last meeting have been passed onto James Archer
			-> ROC meetings are happening
			-> Dustin brought up letter from Pastor Jen regarding RCFL disbanding

	Workgroups (Temporary):	
		- Strategic Plan
			-> Dustin makes motion to create the workgroup, Ryan 2nds, unanimously passes

9:00 Adjourn
	Ryan motions to adjourn
