From exec meeting Dec 2nd. 2024
  - Planned fundraiser delegated tasks
  - Tix prices? What's included with tix?
  - Need "Save the date" flyer/promo material
  - Need to contact businesses to understand prizes/donations
  - Displays of what we do "by the numbers" and/or "through the years"
  - Display of Harrison map + businesses + representation zones
  - Can refer to Facebook to see last fundraiser
  - Start shared Google sheet to track progress/coordination
