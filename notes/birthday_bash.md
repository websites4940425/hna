From exec meeting Dec 2nd. 2024
  - Planned fundraiser delegated tasks
  - Tix prices? What's included with tix?
  - Need "Save the date" flyer/promo material
  - Need to contact businesses to understand prizes/donations
  - Displays of what we do "by the numbers" and/or "through the years"
  - Display of Harrison map + businesses + representation zones
  - Can refer to Facebook to see last fundraiser
  - Start shared Google sheet to track progress/coordination

Meeting Feb 17th 2025
	Raffle Prizes:
		- Currently have:
			Cuppa Java - $50 gift card
			Cuppa Java - Bottle of Wine
			Autodeus Engineering - 90 minute massage gift card 
			Autodeus Engineering - 90 minute massage gift card 
			Henry & Sons - $25 gift card
			Utepils - $25 gift cards
			Utepils - $25 gift cards
			Anon - $15 gift cards
			Anon - $15 gift cards
			Anon - $15 gift cards
			Anon - Jackie Robinson & others picture
			Boys & Girls Club - Gym shoes
			Robert - Prince Picture

		- Still need to gather:
			Misfit Coffee - At office???
			Minne's - Minnepack
			Ryan coordinating:
				Dogs Day Out Dog toy pack
				Alliance Francaise French Summer Class
				Sport Barbershop
				Bus Passes
		- Info
			- 1 bucket each prize		
			- $5 per ticket
			- Ryan announce winners
	
	Timeline of Events:
		12pm La Doña opens
		2:30 Start Setup with volunteers
		4:30-5:30 Social Hour
			5:00 Raffle Announcement
		5:30 Announcements + Board/Staff Acknowledgements
		5:45 Cake
		6:45 Announcements + Board/Staff Acknowledgements
			-> Group Picture
		7:00 - Start Raffle Drawings
		7:30-8:30 - Alto Designo

	Tables:
		Setups:
			- Event Schedule ^^^
			- Sign to point everyone to back
			- General Donation QR codes link
			- Raffle Purchase Table 
				- prize descriptions
				- people walking around pushing tickets
				- phone # on tix
				- Ben help
			- Nichole - HNA through the years from last 35th
			- Ryan - Map w/ streets + representation zones + businesses
				-> push pins Where are you living?
				-> Thomas Printworks?
			- Mitch ?
		Materials (Nichole):
			- Scotch Tape
			- Markers
			- Scissors
			- White Board
			- Table
			- Name Tags

	???'s
		- Setup help time?
		- Sergio/La Doña
			- Sound Setup
			- Table cloths?
			- Drinks tix?
			- What time can we setup?

Mitch help w/ flyers please
