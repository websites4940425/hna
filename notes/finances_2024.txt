=============
CURRENT MONEY
=============
Neighborhoods 2020 (from City of Minneapolis)
49,200 on monthly re-imbursement basis
	-> can get advances
	-> for staff/insurance/supplies
	-> includes:
		network fund -> 15k
		equitable engagement -> 34k
	-> Fall application for following year

NRP funds left
66,282.71
		-> will be less, waiting for reimbursements for exterior/interior
		-> staff funding is ~9346.67

Uncontracted contract (old NRP)
5730.15
	-> TODO Need board vote

Current Check Cash on hand (Statement of Position)
	See current assets Dec. Financial Report
	Accounts receivable - have invoiced, waiting for payment
	Contracts receivable - have not invoiced yet (includes ext/int grant money)
	
===================
POTENTIAL MONEY
===================
Grants	
	DEFINITION housing -> rent control, TOPA (tenant opportunity to purchase), tenant union organizing

	Minneapolis Foundation (Racial Justice Grant)
		$50k
		Due Feb. 15 -> makes decision in March
		Receive April

	McKnight Foundation (General Operation Costs) - Climate Justice grant
		$40k/year for 2 years last time they funded
		Could apply for $50k
		Rolling application period
		~3 months turnaround
		March 14th deadline
	
	Headwaters Transformation	(General Operations) - housing
		$25k/year for 3 years
	
	Emerald Ash bore engagement money Federal
		Unknown, but guess of >$25k
		inflation reduction act	
	
	Emerald Ash bore engagement money State
		admin thru city

	MNDOT owes $3k -> engagement around Olsen Highway redesign

	Metro Blooms
		$20k
		potentially in the next month or 2
		sub-grant 
	
	Northside Funders for bring-back 6	
		in the past $25k

	Blue Line Reparations
		ask for funding to repair diplacement harm	
		$1M for each
			rental assistance,
			home improvement
			down payment assistance
		combo of local, state, federal

	Our Streets
		subgrant potential for bring back 6 work

	Habitat for Humanity???
	Health Department
	Community Safety -> office of violence prevention	
	Green Zone funding

	Shared and collaboration Fund City of Minneapolis
		-> collab with other orgs
		-> heritage park -> Bring back 6
		-> Ariah can help over email	
	
	Partnership Engagement Fund City of Minneapolis
		-> Reedemer
		-> Youth Farm

	Serve as Fiscal Sponsor

Recurring Costs
	Rent
		-> 10,600/year -> lease up in June
		-> New office??? Sub-letting at Park Plaza/Trellis talk to Sunny (E.D. of Laos Assistance Center)
		-> Legal Aid pays 3,600/year
	Computer -> TODO Apple, month-to-month
	Printer -> TODO Make sure we get rid of lease 
	Authnet gateway -> $15/month -> credit card processing -> call bank and block
	Drop Box payment -> 
	Mail Chimp -> current email provider

=============
OTHER
=============
We have about 3 months at our current burn rate according to Robert and Nichole

NRP Policy Board (NCR?) - Who is our neighborhood representative? Need to sort that out
Candidate

To deal with issue of not getting Financial report before meeting:
  * PDF of checking statement (comes in a few days after last day of month)
	* Quickbooks online potentially solution (?) through Tech Soup account (non-profit tech org)
		-> $75 annually
		We could all have access, one person does data entry then Robert would reconcile
		Could connect Quickbooks to bank account directly

Help with grant writing
	-> MN council of non-profits -> workshops
	-> Propel for non-profits -> local
