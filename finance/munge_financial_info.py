from datetime import datetime
import matplotlib.pyplot as plt
import polars as pl

CSV_NAME = "account_history.csv"
ADMIN_COST_STRING_IDENTIFIERS = [
    "Mailchimp",
    "ADP",
    "COMCAST",
    "USBEquipFinance",
    "XCELENERGY",
    "UnitedHealthcare Billing",
    "AUTHNET GATEWAY",
    "ACTION NETWORK T WASHINGTON DC",
    "AMERICAN FAMILY AFT",
    "SQUARESPACE",
    "ADOBE",
    "Adobe",
    "MATCOM PROPE MINNEAPOLIS MN",
    "MAT MINNEAPOLIS MN",
    "DROPBOX",
    "CARON CONSULTING",
    "MN ATTORNEY GENE SAINT PAUL",
    "ELECTIONRUNNER",
]

df = pl.read_csv(CSV_NAME).with_columns(
    pl.col("Post Date").str.to_date(format="%m/%d/%Y").alias("Post Date")
)
df = df.filter(
    pl.col("Post Date").is_between(datetime(2023, 5, 1), datetime(2024, 5, 31)),
)
debits = df.filter(pl.col("Debit").is_not_null())
credits = df.filter(pl.col("Credit").is_not_null())

admin_costs = debits.filter(
    pl.col("Description").str.contains("|".join(ADMIN_COST_STRING_IDENTIFIERS))
)
non_admin_costs = debits.filter(
    ~pl.col("Description").str.contains("|".join(ADMIN_COST_STRING_IDENTIFIERS))
)

#non_admin_costs.write_csv("non_admin_costs.csv")
total_debits = debits.select(pl.sum("Debit")).item()
total_admin_costs = admin_costs.select(pl.sum("Debit")).item()
total_program_costs = non_admin_costs.select(pl.sum("Debit")).item()
total_credits = credits.select(pl.sum("Credit")).item()

print(f"Total Debits: {total_debits:,.2f}")
print(f"Total Admin Costs: {total_admin_costs:,.2f}")
print(f"Total Program Costs: {total_program_costs:,.2f}")
print(f"Total Credits: {total_credits:,.2f}")

# Data to plot
labels = 'Programs', 'Admin Costs'
sizes = [total_program_costs, total_admin_costs]
colors = ['lightblue', 'lightcoral']
explode = (0.1, 0)

# Plot
plt.pie(sizes, explode=explode, labels=labels, colors=colors,
        autopct='%1.1f%%', shadow=True, startangle=140)

plt.axis('equal')  # Equal aspect ratio ensures that pie is drawn as a circle.
plt.title('Total Expenses vs. Program Spending')
plt.figtext(0.5, -0.1, f"Total spending: {total_debits:,.0f}\nAdmin spending: {total_admin_costs:,.0f}", ha="center", fontsize=12, bbox={"facecolor":"orange", "alpha":0.5, "pad":5})
plt.show()
