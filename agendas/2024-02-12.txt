﻿Harrison Neighborhood Association
Monthly Board Meeting
Monday, February 12th 2024 
7pm-9pm

Suggested Agenda from Jan. Executive Committee meeting consisting of Treasurer/Secretary Ryan S. Davis, President Dustin Ruff, and E.D. Nichole Beuhler

7:00 Call to Order/Role Call/Introductions

7:05 Brief Summary of the Standard of Conduct and HNA Mission - Dustin Ruff

7:10 Review and Approve Agenda

7:15 Review and Approve Previous Months Minutes

7:20 Possible re-appointment of board members 

7:30 MPBR Park Improvements Update (Michael Jones)

7:50 Audit client response approval

8:00 ED Report

8:10 Comms Director Report
		TODOs from last time:
			-> link on HNA website for health grants
			-> Centerpoint window kit link on HNA website
			-> figure out Loppet table
			-> setup election runner (did that happen?)

8:20 Committe Structure/Workgroup Updates
	Committees (recurring and consistent):
		- Personnel Committee
		- Events and Communications - Dustin Ruff
		- Finance/Treasurer's Report - Ryan Davis
			-> Met with Nichole and Robert Thompson
				-> gathered a lot of ideas for cost savings, grants
		- Housing and Development - Ben Ptacek

	Workgroups (Temporary):	
		- Audit
		- Bylaws
			-> we passed new bylaws!!!
		- Strategic Plan 
			-> has been tabled recently, shall we resume?

8:25 Adjourn

8:30 CLOSED SESSION for Personnel review
