﻿Harrison Neighborhood Association
Monthly Board Meeting
Monday, July 10th 2023
7pm-9pm

Suggested Agenda from July 6th Executive Committee meeting only consisting of Treasurer/Secretary Ryan S. Davis, President Elect Dustin Ruff, and Executive Director Nichole Buehler (Tim Davis absent)

7:00 Call to Order/Role Call/Introductions

7:05 Reading of the Standard of Conduct and HNA Mission

7:10 Review and Approve Agenda

7:15 Review and Approve Previous Month Minutes

7:20 Treasurer's Report (Robert Thompson Accountant)

7:30 Executive Director's Report

7:35 Board Training

9:00 Adjourn
