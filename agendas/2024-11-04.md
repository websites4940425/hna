﻿Harrison Neighborhood Association
================================================
**Board Meeting Proposed Agenda**
**Monday, Nov. 11th 2024**
**7pm-9pm**

Suggested from Executive Committee meeting consisting of Secretary Ryan S. Davis, President Dustin Ruff, E.D. Nichole Beuhler, and Treasurer Lawrence Graves

# 7:00 Call to Order/Role Call/Introductions

# 7:05 Standard of Conduct and HNA Mission
- Dustin Ruff

# 7:10 Review and Approve Agenda
- Any suggested changes?

# 7:15 Previous Month's Minutes
- Any suggested changes?

# 7:25 Treasurers report
- 2 reports
- 990 approval needed

# 7:35 Michelle Shaw
- Green Zones
- MN Edible Boulevards

# 8:00 ED Report

# 8:25 Committee Structure/Workgroup Updates
## Committees (recurring and consistent):
- Events and Communications - Dustin Ruff
	- 40th fundraiser updates
	- Supper Time at Alliance Frances coming up
- Earlier Finance/Treasurer's Report - Lawrence Graves
- Housing and Development - Ben Ptacek
	- EyeBobs not renewing lease
	- Half-Time Sport Barber Shop Open!

## Workgroups (temporary):	
- Personnel Review
- Strategic Plan Workgroup
  - Tabled until someone takes initiative

# 9:00 Adjourn
