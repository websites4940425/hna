﻿Harrison Neighborhood Association
================================================
**Board Meeting Proposed Agenda**
**Monday, Jan. 27th 2025**
**7pm-9pm**

Suggested from Executive Committee meeting consisting of Secretary Ryan S. Davis, President Dustin Ruff, E.D. Nichole Beuhler, and Treasurer Lawrence Graves

# 7:00 Call to Order/Role Call/Introductions

# 7:05 Standard of Conduct and HNA Mission
- Dustin Ruff

# 7:10 Review and Approve Agenda
- Any suggested changes?

# 7:15 Previous Month's Minutes
- Any suggested changes?

# 7:25 Treasurers report
- 2025 Organization Budget
- Approval to allocate $5,730.15 of NRP for administrative expenses

# 7:55 ED Report
- Trellis dev updates
- Update on newly secured funding

# 8:20 Committee Structure/Workgroup Updates
## Committees (recurring and consistent):
- Events and Communications - Dustin Ruff
	- 40th Birthday Bash updates, Feb 22nd 4:30-8:30
- Housing and Development - Ben Ptacek
- Finance/Treasurer's Report - Lawrence Graves -> see earlier

## Workgroups (temporary):	
- Personnel Review
- Strategic Plan Workgroup
	- Dustin had spoken with someone

# 8:30 Closed Session
- ED Review
	- We will have sent out email to Nichole

# 9:00 Adjourn
